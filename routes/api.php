<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['prefix'=>'courses'], function() {
  Route::get('/', 'CoursesController@index')->name('courses.index');
  Route::get('/common', 'CoursesController@common');
  Route::get('/spec', 'CoursesController@spec');
  Route::get('/choice', 'CoursesController@choice');

  Route::post('/update', 'CoursesController@update')->name("grade.update");
});

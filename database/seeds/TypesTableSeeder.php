<?php

use Illuminate\Database\Seeder;
use App\Type;

class TypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Type::insert([
          [
            'name' => 'common'
          ],
          [
            'name' => 'choice'
          ],
          [
            'name' => 'specialization'
          ],
          [
            'name' => 'minor/stage'
          ],
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;
use App\Course;

class CoursesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Voeg mediatech vakken toe
        Course::insert([
          [
            'name' => 'IMTPMD',
            'ects' => 3,
            'grade' => 0.0,
            'type_id' => 3,
          ],
          [
            'name' => 'IMTD1',
            'ects' => 3,
            'grade' => 0.0,
            'type_id' => 3,
          ],
          [
            'name' => 'IMTCM',
            'ects' => 3,
            'grade' => 0.0,
            'type_id' => 3,
          ],
          [
            'name' => 'IMTUE',
            'ects' => 3,
            'grade' => 0.0,
            'type_id' => 3,
          ],
          [
            'name' => 'IPMEDT2',
            'ects' => 6,
            'grade' => 0.0,
            'type_id' => 3,
          ],
          [
            'name' => 'IPMEDT3',
            'ects' => 6,
            'grade' => 0.0,
            'type_id' => 3,
          ],
          [
            'name' => 'IPMEDT4',
            'ects' => 6,
            'grade' => 0.0,
            'type_id' => 3,
          ],
          [
            'name' => 'IPMEDT5',
            'ects' => 6,
            'grade' => 0.0,
            'type_id' => 3,
          ],
          [
            'name' => 'IPMEDTH',
            'ects' => 9,
            'grade' => 0.0,
            'type_id' => 3,
          ],
          [
            'name' => 'IMTUX',
            'ects' => 3,
            'grade' => 0.0,
            'type_id' => 3,
          ],
          [
            'name' => 'IMTHMI',
            'ects' => 3,
            'grade' => 0.0,
            'type_id' => 3,
          ],
          [
            'name' => 'IMTHE1',
            'ects' => 3,
            'grade' => 0.0,
            'type_id' => 3,
          ],
        ]);

        // Voeg hoofdfase algemene vakken toe
        Course::insert([
          [
            'name' => 'IRDBMS',
            'ects' => 3,
            'grade' => 0.0,
            'type_id' => 1,
          ],
          [
            'name' => 'ISCRIPT',
            'ects' => 3,
            'grade' => 0.0,
            'type_id' => 1,
          ],
          [
            'name' => 'IQUA',
            'ects' => 3,
            'grade' => 0.0,
            'type_id' => 1,
          ],
          [
            'name' => 'IETH',
            'ects' => 3,
            'grade' => 0.0,
            'type_id' => 1,
          ],
          [
            'name' => 'ICOMMH',
            'ects' => 3,
            'grade' => 0.0,
            'type_id' => 1,
          ],
          [
            'name' => 'ISLH',
            'ects' => 3,
            'grade' => 0.0,
            'type_id' => 1,
          ],
          [
            'name' => 'IITORG',
            'ects' => 3,
            'grade' => 0.0,
            'type_id' => 1,
          ],
          [
            'name' => 'ISEC',
            'ects' => 3,
            'grade' => 0.0,
            'type_id' => 1,
          ],
        ]);

        // Voeg keuzevakken toe
        Course::insert([
          [
            'name' => 'IDEPA',
            'ects' => 3,
            'grade' => 0.0,
            'type_id' => 2,
          ],
          [
            'name' => 'IKOPR',
            'ects' => 3,
            'grade' => 0.0,
            'type_id' => 2,
          ],
          [
            'name' => 'IITPS',
            'ects' => 3,
            'grade' => 0.0,
            'type_id' => 2,
          ],
          [
            'name' => 'IFRAME',
            'ects' => 3,
            'grade' => 0.0,
            'type_id' => 2,
          ],
          [
            'name' => 'IKEMA',
            'ects' => 3,
            'grade' => 0.0,
            'type_id' => 2,
          ],
        ]);

        // Voeg propedeuse vakken toe
        Course::insert([
          [
            'name' => 'IARCH',
            'ects' => 3,
            'grade' => 0.0,
            'type_id' => 1,
          ],
          [
            'name' => 'IIBPM',
            'ects' => 3,
            'grade' => 0.0,
            'type_id' => 1,
          ],
          [
            'name' => 'IHBO',
            'ects' => 3,
            'grade' => 0.0,
            'type_id' => 1,
          ],
          [
            'name' => 'IOPR1',
            'ects' => 4,
            'grade' => 0.0,
            'type_id' => 1,
          ],
          [
            'name' => 'IWDR',
            'ects' => 3,
            'grade' => 0.0,
            'type_id' => 1,
          ],
          [
            'name' => 'IRDB',
            'ects' => 3,
            'grade' => 0.0,
            'type_id' => 1,
          ],
          [
            'name' => 'IIBUI',
            'ects' => 3,
            'grade' => 0.0,
            'type_id' => 1,
          ],
          [
            'name' => 'IPODM',
            'ects' => 2,
            'grade' => 0.0,
            'type_id' => 1,
          ],
          [
            'name' => 'IPOMEDT',
            'ects' => 2,
            'grade' => 0.0,
            'type_id' => 1,
          ],
          [
            'name' => 'IMUML',
            'ects' => 3,
            'grade' => 0.0,
            'type_id' => 1,
          ],
          [
            'name' => 'IOPR2',
            'ects' => 4,
            'grade' => 0.0,
            'type_id' => 1,
          ],
          [
            'name' => 'IFIT',
            'ects' => 3,
            'grade' => 0.0,
            'type_id' => 1,
          ],
          [
            'name' => 'IPOFIT',
            'ects' => 2,
            'grade' => 0.0,
            'type_id' => 1,
          ],
          [
            'name' => 'IPOSE',
            'ects' => 2,
            'grade' => 0.0,
            'type_id' => 1,
          ],
          [
            'name' => 'INET',
            'ects' => 3,
            'grade' => 0.0,
            'type_id' => 1,
          ],
          [
            'name' => 'IPROV',
            'ects' => 3,
            'grade' => 0.0,
            'type_id' => 1,
          ],
          [
            'name' => 'ICOMMP',
            'ects' => 3,
            'grade' => 0.0,
            'type_id' => 1,
          ],
          [
            'name' => 'ISLP',
            'ects' => 1,
            'grade' => 0.0,
            'type_id' => 1,
          ],
          [
            'name' => 'IIPMEDT',
            'ects' => 10,
            'grade' => 0.0,
            'type_id' => 3,
          ],

        ]);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Course;
use App\Type;
use App\User;

class CoursesController extends Controller
{
    public function index() {
      $courses = Course::all();
      return $courses;
    }

    public function common() {
      $type = Type::where('id',1)->first();
      $courses = Course::where('type_id',$type->id)->get();

      return $courses;
    }

    public function spec() {
      $type = Type::where('id',3)->first();
      $courses = Course::where('type_id',$type->id)->get();

      return $courses;
    }

    public function choice() {
      $type = Type::where('id',2)->first();
      $courses = Course::where('type_id',$type->id)->get();

      return $courses;
    }

    public function update(Request $json) {
      $grade = floatval($json->grade);
      $id = $json->id;

      Course::find($id)->update(['grade' => $grade]);
      $course = Course::find($id);
      return $course;
    }
}

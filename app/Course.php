<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $fillable = ['name', 'ects', 'grade','type_id'];

    public $timestamps;

    public function type() {
      return $this->hasOne('App\Type');
    }

    public function findCoursesByType($type_id) {
      return Course::where('type_id',$type_id);
    }

    public function users() {
      return $this->belongsToMany('App\User', 'course_user');
    }
}
